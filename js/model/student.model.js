function Student(_id, _name, _pass, _email, _math, _physics, _chem) {
  this._id = _id;
  this._name = _name;
  this._pass = _pass;
  this._email = _email;
  this._math = _math;
  this._physics = _physics;
  this._chem = _chem;
}

Student.prototype.getAvg = function () {
  let student_math = parseFloat(this._math) || 0;
  let student_physics = parseFloat(this._physics) || 0;
  let student_chem = parseFloat(this._chem) || 0;
  return Math.floor((student_math + student_physics + student_chem) / 3);
};
