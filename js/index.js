let student_list = load_local_storage("student_list");
if (student_list.length) {
  for (let i = 0; i < student_list.length; i++) {
    let current_student = student_list[i];
    student_list[i] = new Student(
      current_student._id,
      current_student._name,
      current_student._pass,
      current_student._email,
      current_student._math,
      current_student._physics,
      current_student._chem
    );
  }

  render_html_data("#tbodySinhVien", student_list);
}

document.getElementById("addSV").addEventListener("click", () => {
  add_student();
});

document.getElementById("clearTable").addEventListener("click", () => {
  clear_form_data();
});

document.getElementById("btnSearch").addEventListener("click", () => {
  let search_name_val = document.getElementById("txtSearch").value;
  search_student_by_name(search_name_val);
});

document.getElementById("updateTable").addEventListener("click", (e) => {
  console.log(e.target.dataset.index);
  let student_idx = e.target.dataset.index;
  if (student_idx === undefined) {
    console.log("Nothing to update");
    return;
  }
  update_student(student_idx);
});
