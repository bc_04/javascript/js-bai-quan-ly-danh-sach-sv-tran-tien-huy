let get_form_data = () => {
  let student_id = document.getElementById("txtMaSV").value;
  let student_name = document.getElementById("txtTenSV").value;
  let student_email = document.getElementById("txtEmail").value;
  let student_pass = document.getElementById("txtPass").value;
  let student_math = document.getElementById("txtDiemToan").value;
  let student_physics = document.getElementById("txtDiemLy").value;
  let student_chemistry = document.getElementById("txtDiemHoa").value;
  return new Student(
    student_id,
    student_name,
    student_pass,
    student_email,
    student_math,
    student_physics,
    student_chemistry
  );
};

let set_form_data = (student_obj) => {
  document.getElementById("txtMaSV").value = student_obj._id;
  document.getElementById("txtTenSV").value = student_obj._name;
  document.getElementById("txtEmail").value = student_obj._email;
  document.getElementById("txtPass").value = student_obj._pass;
  document.getElementById("txtDiemToan").value = student_obj._math;
  document.getElementById("txtDiemLy").value = student_obj._physics;
  document.getElementById("txtDiemHoa").value = student_obj._chem;
};

let clear_form_data = () => {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
};

let render_html_data = (target_el, data) => {
  let render_obj = new Render_Helper();
  render_obj.set_target_el(target_el);
  render_obj.set_target_data(data);
  render_obj.render_html();
};

let find_student_by_id = (id) => {
  return student_list.findIndex((student) => student._id === id);
};

let find_student_by_name = (name) => {
  return student_list.findIndex((student) => student._name === name);
};

let add_student = () => {
  let student_obj = get_form_data();
  let form_validation_obj = new Form_Validation_Controller(
    student_obj,
    student_list
  );
  console.log(student_obj);
  console.log("validation:", form_validation_obj.handle_validation());
  if (form_validation_obj.handle_validation()) {
    student_list.push(student_obj);
    save_local_storage("student_list", student_list);
    render_html_data("#tbodySinhVien", student_list);
  }
};

let delete_student = (id) => {
  let student_to_update_index = find_student_by_id(id);
  if (student_to_update_index > -1) {
    student_list.splice(student_to_update_index, 1);
    del_local_storage("student_list");
    if (student_list.length) {
      save_local_storage("student_list", student_list);
    }
    render_html_data("#tbodySinhVien", student_list);
  }
};

let edit_student = (id) => {
  let student_to_update_index = student_list.findIndex(
    (student) => student._id === id
  );
  if (student_to_update_index > -1) {
    set_form_data(student_list[student_to_update_index]);
    document.getElementById("txtMaSV").setAttribute("disabled", "");
    document.getElementById("addSV").setAttribute("disabled", "");
    document
      .getElementById("updateTable")
      .removeAttribute("data-index");
    document
      .getElementById("updateTable")
      .setAttribute("data-index", student_to_update_index);
  }
};

let update_student = (index) => {
  let new_student_obj = get_form_data();
  let form_validation_obj = new Form_Validation_Controller(
    new_student_obj,
    student_list
  );
  form_validation_obj.check_duplicate = false;
  if (form_validation_obj.handle_validation()) {
    student_list[index] = new_student_obj;
    document.getElementById("txtMaSV").removeAttribute("disabled");
    document.getElementById("addSV").removeAttribute("disabled");
    document.getElementById("updateTable").removeAttribute("data-index");
    clear_form_data();
    del_local_storage("student_list");
    save_local_storage("student_list", student_list);
    render_html_data("#tbodySinhVien", student_list);
  }
};

let search_student_by_name = (name) => {
  let find_index = find_student_by_name(name);
  if (find_index > -1) {
    searched_student = student_list[find_index];
    render_html_data("#tbodySinhVien", [searched_student]);
  } else {
    render_html_data("#tbodySinhVien");
  }
};
