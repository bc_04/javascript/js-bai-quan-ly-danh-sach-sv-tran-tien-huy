function Validator_Helper(_value, _target) {
  this._value = _value;
  this._target = _target;
  this._message = "";
  this.is_empty = is_empty;
  this.is_smaller = is_smaller;
  this.is_greater = is_greater;
  this.is_valid_email = is_valid_email;
  this.is_exist = is_exist;
  const REGEX_EMAIL =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  function set_err_msg(msg) {
    this._message = msg;
  }

  function get_err_msg() {
    return this._message;
  }

  function is_empty(msg) {
    set_err_msg(msg);
    if (this._value.length) {
      set_err_msg("");
    }
    document.querySelectorAll(this._target)[0].innerText = get_err_msg();
    return this._value.length;
  }

  function is_exist(msg, data_list = [], key) {
    set_err_msg(msg);

    let find_index = data_list.findIndex(
      (student) => student[key] === this._value
    );
    if (find_index == -1) {
      set_err_msg("");
    }
    document.querySelectorAll(this._target)[0].innerText = get_err_msg();
    return find_index;
  }

  function is_smaller(msg, min) {
    set_err_msg(msg);
    this._value.length >= min && set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = get_err_msg();
    return this._value.length >= min;
  }

  function is_greater(msg, max) {
    set_err_msg(msg);
    this._value.length <= max && set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = get_err_msg();
    return this._value.length <= max;
  }

  function is_valid_email(msg) {
    set_err_msg(msg);
    REGEX_EMAIL.test(this._value) && set_err_msg("");
    document.querySelectorAll(this._target)[0].innerText = get_err_msg();
    return REGEX_EMAIL.test(this._value);
  }
}
