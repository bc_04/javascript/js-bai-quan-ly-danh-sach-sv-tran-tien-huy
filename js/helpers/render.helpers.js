function Render_Helper() {
  let target = "";
  let target_data = "";
  this.set_target_el = (target_el) => {
    target = target_el;
  };
  this.get_target_el = () => {
    return target;
  };

  this.set_target_data = (data) => {
    target_data = data;
  };

  this.get_target_data = (data) => {
    return target_data;
  };
  this.clear_content = function () {
    document.querySelectorAll(this.get_target_el())[0].innerHTML = "";
  };
  this.create_html_template = function () {
    let html_template = "";
    let data = this.get_target_data();
    data.forEach((student) => {
      html_template += `
          <tr class="student student-${student._id}">
              <td>${student._id}</td>
              <td>${student._name}</td>
              <td>${student._email}</td>
              <td>${student.getAvg()}</td>
              <td class="action">
                  <i class="icon icon-edit fa-solid fa-pen-to-square" 
                  data-target="${student._id}"></i>
                  <i class="icon icon-del fa-solid fa-xmark" 
                  data-target="${student._id}"></i>
              </td>
          </tr>
      `;
    });
    return html_template;
  };
}

Render_Helper.prototype.render_html = function () {
  this.clear_content();
  document.querySelectorAll(this.get_target_el())[0].innerHTML =
    this.create_html_template();
  document.querySelectorAll(".icon-edit").forEach((edit_btn) => {
    edit_btn.addEventListener("click", () => {
      edit_student(edit_btn.dataset.target);
    });
  });
  document.querySelectorAll(".icon-del").forEach((del_btn) => {
    del_btn.addEventListener("click", () => {
      delete_student(del_btn.dataset.target);
    });
  });
};
